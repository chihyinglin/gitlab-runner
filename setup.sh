#!/bin/bash

img=registry.gitlab.com/chihyinglin/gitlab-runner:latest
runner_name=$RUNNER_NAME
gitlab_url=$GITLAB_URL
gitlab_reg_token=$GITLAB_REG_TOKEN


## executor: shell
docker run -it --rm \
       -v `pwd`/config:/etc/gitlab-runner \
       -v /var/run/docker.sock:/var/run/docker.sock \
       ${img} \
       register \
       --non-interactive \
       --url ${gitlab_url} \
       --registration-token ${gitlab_reg_token} \
       --locked="false" \
       --executor shell \
       --description ${runner_name}-shell-`hostname` \
       --tag-list "${runner_name}-shell"

## executor: docker
docker run -it --rm \
       -v `pwd`/config:/etc/gitlab-runner \
       -v /var/run/docker.sock:/var/run/docker.sock \
       ${img} \
       register \
       --non-interactive \
       --url ${gitlab_url} \
       --registration-token ${gitlab_reg_token} \
       --locked="false" \
       --executor docker \
       --description ${runner_name}-docker-`hostname` \
       --tag-list "${runner_name}-docker" \
       --docker-image docker:19.03.12


# update concurrent number based on the cpu count
sudo sed -i 's/^concurrent.*$/concurrent = '$(( `nproc --all` * 4 ))'/g' config/config.toml

# run runner container
docker rm -f gitlab-runner-${runner_name}
docker run \
        -d  \
        --restart always \
        --name gitlab-runner-${runner_name} \
        -v `pwd`/config:/etc/gitlab-runner \
        -v /var/run/docker.sock:/var/run/docker.sock \
        ${img} \


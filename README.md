# gitlab-runner
- Script to run a gitlab runner in docker(share the host's docker engine)
- Setup two executor type of runner
  - shell
  - docker

## usage
### setup runner
1. the registered info stored in config folder, remove it if need 
    - `sudo rm -rf config/*`
2. get the parameters
    - **GITLAB_URL** / **GITLAB_REG_TOKEN**
      - from `gitlab project->settings->CI/CD->Runners-Specific Runners`
    - **RUNNER_NAME**: user specific name
3. run the script
    ```
    RUNNER_NAME=my_runner \
    GITLAB_URL=https://gitlab.com/ \
    GITLAB_REG_TOKEN=xxxxx \
    ./setup.sh
    ```
### use it in pipeline
- `.gitlab-ci.yml`
  - ```
    stages:
    - build
    
    build-img:
      stage: build
      tags:
        - my_runner-shell  # or my_runner-docker
      script:
        - # docker build ...
    ```
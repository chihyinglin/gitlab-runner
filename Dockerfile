FROM gitlab/gitlab-runner:v13.0.1

# install packages
RUN apt-get update && apt-get install -y docker.io && apt-get clean

CMD ["run"]
